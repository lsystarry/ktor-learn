package com.example

/**
 * @author lsy
 * @date 2021/4/20 11:45
 */
data class BlogEntry(val headline: String, val body: String)

val blogEntries = mutableListOf(BlogEntry(
    "The drive to develop!",
    "...it's what keeps me going."
))
