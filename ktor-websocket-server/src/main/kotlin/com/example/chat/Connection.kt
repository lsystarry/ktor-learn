package com.example.chat

import io.ktor.http.cio.websocket.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * @author lsy
 * @date 2021/4/19 22:10
 */
class Connection(val session: DefaultWebSocketSession) {

    companion object {
        var lastId = AtomicInteger(0)
    }

    val name = "user${lastId.getAndIncrement()}"
}