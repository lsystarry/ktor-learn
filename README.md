# ktor-learn

ktor框架学习项目，参考官网：https://ktor.io/docs/welcome.html

- ktor-sample

  简单RESTful风格应用

- ktor-websit

  简单web应用

- ktor-websocket-client

  简单多人聊天应用客户端，结合websocket

- ktor-websocket-server

  简单多人聊天应用服务端，结合websocket