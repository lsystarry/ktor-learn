package com.lsy.core

import io.ktor.routing.*

/**
 * @author lsy
 * @date 2021/4/20 20:46
 */
interface CustomRoute {

    fun Route.route()
}