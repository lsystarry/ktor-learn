package com.lsy.core

import com.lsy.dao.AdminDao
import com.lsy.service.AdminService
import com.lsy.service.impl.AdminServiceImpl
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.kodein.di.*
import org.ktorm.database.Database
import org.ktorm.logging.ConsoleLogger
import org.ktorm.logging.LogLevel


val totalDI = DI.lazy {
    import(dbModule, true)
    import(daoModule, true)
    import(serviceModule, true)
}

val db by totalDI.newInstance { instance<Database>() }

/**
 * 数据库连接池管理
 */
val dbModule = DI.Module("dbModule") {
    bind<HikariDataSource>() with singleton {
        HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:mysql://115.159.95.164:3306/one-mall"
            driverClassName = "com.mysql.cj.jdbc.Driver"
            username = "root"
            password = "123456"
            maximumPoolSize = 30
            minimumIdle = 8
        })
    }
    bind<Database>() with singleton {
        Database.connect(
            dataSource = instance<HikariDataSource>(),
            logger = ConsoleLogger(threshold = LogLevel.DEBUG)
        )
    }
}

val daoModule = DI.Module("daoModule") {
    bind<AdminDao>() with singleton { AdminDao() }
}

val serviceModule = DI.Module("serviceModule") {
    bind<AdminService>() with singleton { AdminServiceImpl() }
}