package com.lsy.service

import com.lsy.model.Admin
import org.ktorm.dsl.Query

/**
 * @author lsy
 * @date 2021/4/20 18:51
 */
interface AdminService {

    fun findInfo(id: Long): Admin?

    fun query(): List<Admin>
}