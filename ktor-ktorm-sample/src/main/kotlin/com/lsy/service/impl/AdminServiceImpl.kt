package com.lsy.service.impl

import com.lsy.dao.AdminDao
import com.lsy.model.Admin
import com.lsy.core.totalDI
import com.lsy.service.AdminService
import org.kodein.di.instance
import org.ktorm.dsl.Query

/**
 * @author lsy
 * @date 2021/4/20 18:52
 */
class AdminServiceImpl : AdminService {

    private val adminService by totalDI.instance<AdminDao>()

    override fun findInfo(id: Long): Admin? {
        return adminService.findAdmin(id)
    }

    override fun query(): List<Admin> = adminService.query()
}