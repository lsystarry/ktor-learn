package com.lsy.controller

import com.lsy.component.R
import com.lsy.core.CustomRoute
import com.lsy.core.totalDI
import com.lsy.service.AdminService
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.instance

/**
 * @author lsy
 * @date 2021/4/20 20:46
 */
class AdminApi : CustomRoute{

    private val targetService by totalDI.instance<AdminService>()

    override fun Route.route() {
        route("/admin") {
            get("/info/{id}") {
                val id = call.parameters["id"]
                val admin = targetService.findInfo(id!!.toLong())
                call.respond(status = HttpStatusCode.OK, R.ok(admin))
            }
            get("/query") {
                call.respond(status = HttpStatusCode.OK, R.ok(targetService.query()))
            }
        }
    }
}
