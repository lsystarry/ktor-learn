package com.lsy.model

import org.ktorm.entity.Entity
import java.time.LocalDateTime

/**
 * @author lsy
 * @date 2021/4/20 18:36
 */
interface Admin : Entity<Admin> {
    companion object : Entity.Factory<Admin>()
    val id: Long?
    val username: String?
    val password: String?
    val createTime: LocalDateTime?
    val updateTime: LocalDateTime?
}