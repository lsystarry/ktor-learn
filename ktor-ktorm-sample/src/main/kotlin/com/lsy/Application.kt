package com.lsy

import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.SerializationFeature
import com.lsy.config.LocalDateTimeModule
import com.lsy.core.CustomRoute
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.jackson.*
import io.ktor.routing.*
import org.ktorm.jackson.KtormModule
import org.reflections.Reflections
import kotlin.reflect.full.functions

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)


fun Application.module() {
    //configureSecurity()
    install(CallLogging)
    install(DefaultHeaders)
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            this.registerModules(modules)
        }
    }
    val reflections = Reflections("com.lsy.controller")
    val list = reflections.getSubTypesOf(CustomRoute::class.java)
    if (list.isNotEmpty()) {
        list.forEach { clazz ->
            clazz.kotlin.functions.find {
                it.name == "route"
            }?.call(clazz.getDeclaredConstructor().newInstance(), routing {  })
        }
    }
}

val modules = mutableListOf<Module>(KtormModule(), LocalDateTimeModule())