package com.lsy.component

import java.time.LocalDateTime

/**
 * @author lsy
 * @date 2021/4/20 20:49
 */
class R<T> {

    var code: Int? = null
    var timestamp: LocalDateTime? = null
    var message: String? = null
    var data: T? = null

    companion object {

        fun <T> ok(data: T?): R<T> = resultApi(200, LocalDateTime.now(), null, data)

        fun <T> failure(message: String?): R<T> = failure(500, message)

        fun <T> failure(code: Int?, message: String?): R<T> = resultApi(code, LocalDateTime.now(), message, null)

        private fun <T> resultApi(code: Int?, timestamp: LocalDateTime?, message: String?, data: T?): R<T> {
            val resultApi = R<T>()
            resultApi.code = code
            resultApi.timestamp = timestamp
            resultApi.message = message
            resultApi.data = data
            return resultApi
        }
    }
}