package com.lsy.config

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.json.PackageVersion
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.module.SimpleModule
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * @author lsy
 * @date 2021/4/20 21:48
 */
class LocalDateTimeModule : SimpleModule(PackageVersion.VERSION) {

    init {
        addSerializer(LocalDateTime::class.java, TsLocalDateTimeSerializer())
        addDeserializer(LocalDateTime::class.java, TsLocalDateTimeDeserializer())
    }

    companion object {
        internal class TsLocalDateTimeSerializer : JsonSerializer<LocalDateTime>() {
            override fun serialize(value: LocalDateTime?, gen: JsonGenerator?, serializers: SerializerProvider?) {
                val dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                gen!!.writeString(dtf.format(value))
            }
        }

        internal class TsLocalDateTimeDeserializer : JsonDeserializer<LocalDateTime>() {
            override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): LocalDateTime? {
                val string = p?.valueAsString
                if (null == string || string.isBlank()) {
                    return null
                }
                val dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                return LocalDateTime.parse(string, dtf)
            }
        }
    }
}