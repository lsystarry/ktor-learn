package com.lsy.dao.tablebind

import com.lsy.model.Admin
import org.ktorm.schema.*

/**
 * @author lsy
 * @date 2021/4/20 18:42
 */
object Admins : Table<Admin>("tb_admin") {

    val id = long("id").primaryKey().bindTo { it.id }
    val username = varchar("user_name").bindTo { it.username }
    val password = varchar("password").bindTo { it.password }
    val createTime = datetime("create_time").bindTo { it.createTime }
    val updateTime = datetime("update_time").bindTo { it.updateTime }
}