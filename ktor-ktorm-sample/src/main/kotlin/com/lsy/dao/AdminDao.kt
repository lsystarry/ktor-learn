package com.lsy.dao

import com.lsy.dao.tablebind.Admins
import com.lsy.model.Admin
import com.lsy.core.db
import org.ktorm.database.Database
import org.ktorm.dsl.Query
import org.ktorm.dsl.eq
import org.ktorm.entity.*

/**
 * @author lsy
 * @date 2021/4/20 20:17
 */
class AdminDao {

    private val Database.admin get() = this.sequenceOf(Admins)

    fun findAdmin(id: Long): Admin? = db.admin.find { it.id eq id }

    fun query(): List<Admin> = db.admin.toList()
}