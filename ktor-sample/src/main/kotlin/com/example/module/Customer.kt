package com.example.module

import kotlinx.serialization.Serializable

/**
 * @author lsy
 * @date 2021/4/19 16:45
 */
@Serializable
data class Customer(val id: String, val firstName: String, val lastName: String, val email: String)

val customerStorage = mutableListOf<Customer>()